pkg = require './package.json'

module.exports = (grunt) ->

  for taskName of pkg.devDependencies
    if taskName.substring(0, 6) == 'grunt-' then grunt.loadNpmTasks taskName

  grunt.task.loadNpmTasks 'assemble'

  config =

    pkg: grunt.file.readJSON('package.json')

    # assemble:
    #   site:
    #     options: 
    #       data: ['config.yml']
    #     files: [
    #       src: 'src/pages/1.hbs'
    #       dest: 'dest/1.html'
    #     ]

    # サイト全体を管理
    # assemble:
    #   site:
    #     options: 
    #       data: ['config.yml']
    #     files: [
    #       expand : true
    #       cwd    : 'src/pages/'
    #       src    : '**/*.hbs'
    #       dest   : 'dest/'
    #     ]

    # パーシャルファイル
    # assemble:
    #   site:
    #     options: 
    #       data    : ['config.yml']
    #       partials: 'src/partials/**/*.hbs'
    #     files: [
    #       expand : true
    #       cwd    : 'src/pages/'
    #       src    : '**/*.hbs'
    #       dest   : 'dest/'
    #     ]

    # レイアウト機能
    # assemble:
    #   site:
    #     options: 
    #       data     : ['config.yml']
    #       layout   : 'src/layouts/default.hbs'
    #       partials : 'src/partials/**/*.hbs'
    #     files: [
    #       expand : true
    #       cwd    : 'src/pages/'
    #       src    : '**/*.hbs'
    #       dest   : 'dest/'
    #     ]

    # アセット機能
    # assemble:
    #   site:
    #     options: 
    #       data     : ['config.yml']
    #       layout   : 'src/layouts/default.hbs'
    #       partials : 'src/partials/**/*.hbs'
    #     files: [
    #       expand : true
    #       cwd    : 'src/pages/'
    #       src    : '**/*.hbs'
    #       dest   : 'dest/'
    #     ]

    # 最適化
    # assemble:
    #   dev:
    #     options: 
    #       data       : ['config.yml']
    #       layout     : 'src/layouts/default.hbs'
    #       partials   : 'src/partials/**/*.hbs'
    #       dev        : true
    #       production : false
    #     files: [
    #       expand : true
    #       cwd    : 'src/pages/'
    #       src    : '**/*.hbs'
    #       dest   : 'dest/'
    #     ]
    #   production:
    #     options: 
    #       data       : ['config.yml']
    #       layout     : 'src/layouts/default.hbs'
    #       partials   : 'src/partials/**/*.hbs'
    #       dev        : false
    #       production : true
    #     files: [
    #       expand : true
    #       cwd    : 'src/pages/'
    #       src    : '**/*.hbs'
    #       dest   : 'dest/'
    #     ]

    # ナビゲーションのカレント
    assemble:
      dev:
        options: 
          data     : ['nav.yml']
          layout   : 'src/layouts/default.hbs'
          partials : 'src/partials/**/*.hbs'
        files: [
          expand : true
          cwd    : 'src/pages'
          src    : '**/*.hbs'
          dest   : 'dest/'
        ]

    copy:
      assets:
        expand : true
        cwd    : 'src/assets'
        src    : '**'
        dest   : 'dest/assets'

  grunt.initConfig( config )

  grunt.registerTask 'default', []

  grunt.registerTask 'build', [
    'assemble:dev'
    'copy'
  ]

  grunt.registerTask 'build:production', [
    'assemble:production'
    'copy'
  ]